<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:template match="/JobNames">
	<html>
		<head> 
		<title>JobNames</title> 
<style>
table {
    width:100%;
	margin-top: 15px;
    margin-bottom: 10px;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
.btitle {
    font-weight: bold;
}
</style>
		</head>
		<body>
			<span class="btitle"> Scheduled Job's Details : </span> 
			<table>
			<!-- grid header -->
			<tr bgcolor="lightgrey">
				<td>Job Name</td>
				<td>Scheduled At</td>
				<td>Status</td>
				<td>Start Date</td>
				<td>End Date</td>
				<td>Errors</td>
			</tr>
				<xsl:apply-templates select="JobName">
				<xsl:sort select="title" />
				</xsl:apply-templates>
			</table>
		</body>
	</html>
</xsl:template>
<xsl:template match="JobName">
	<!-- grid value fields -->
	<tr>
		<td><xsl:value-of select="JobName"/></td>
		<td><xsl:value-of select="ScheduledAt"/></td>
		<td><xsl:value-of select="Status"/></td>
		<td><xsl:value-of select="StartDate"/></td>
		<td><xsl:value-of select="EndDate"/></td>
		<td><xsl:value-of select="Errors"/></td>
	</tr>
</xsl:template>
</xsl:stylesheet>